#include <iostream>
//#include "pessoa.hpp"
#include "cliente.hpp"
#include "funcionario.hpp"
#include "socio.hpp"
#include "produto.hpp"
#include "funcoes.hpp"
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

Produto * Menu_produtos(vector <Produto*> produtos){
	for(auto produto:produtos)
		produto->imprime_dados();
	string nome_produto;
	cout << "Digite o nome do produto que deseja adquirir: (Utilize um '_', caso seja um nome composto)" << endl;
	cin >> nome_produto;
	nome_produto = "Produto: " + nome_produto;
	for(auto produto:produtos){
		if(!nome_produto.compare(produto->get_nome())){
			cout << "Digite a quantidade que deseja adquirir:" << endl;
			string qtd;
			cin >> qtd;
			produto->set_quantidade(qtd);
			return produto;
		}
	}
	cout << "Produto não encontrado" << endl;
	return NULL;
}

void Interface(Cliente * cliente, vector <Produto*> produtos){
	cout << "Total: R$" << cliente->calcular_compras() << endl;
	cout << "1. Compra\n2. Recomendações\n3. Voltar ao menu principal" << endl;
	int opcoes;
	cin >> opcoes;
	switch(opcoes){
		case 1:
			cliente->inserir_produtos(Menu_produtos(produtos));
			Interface(cliente, produtos);
		break;
		case 2:
			
		break;
		default:
			
		break;
	}
}

void Atualiza_estoque(){
	
	fstream doc_estoque;
	doc_estoque.open("doc/estoque.txt", ios::out | ios::in | ios::app);
	
	if(doc_estoque.is_open()){
	
		string linhaf;
		cout << "Digite o nome do novo produto:" << endl;
		cin >> linhaf;
		
		int qtd;
		cout << "Digite a quantidade a ser adicionada:" << endl;
		cin >> qtd;
		
		string linha;
		while (getline(doc_estoque, linha)){
			cout << linha << endl;
			if(!linhaf.compare(linha)){
				
				cout << "Quantidade do produto atualizada" << endl;
				return;
			}
		}
		
		string preco;
		cout << "Digite o preço do produto:" << endl;
		cin >> preco;
		string cat;
				
		doc_estoque.close();
		doc_estoque.open("doc/estoque.txt", ios::out | ios::in | ios::app);
		doc_estoque << "Produto: " <<linhaf << endl;
		doc_estoque << "Quantidade: " << qtd << endl;
		doc_estoque << "Preço: " << preco << endl;
		int opt = 1;
		while(opt){
			cout << "1. Adicionar Categoria" << endl;
			cout << "2. Fechar produto" << endl;
			cin >> opt;
			switch (opt){
				case 1:
					cout << "Digite o nome da categoria:" << endl;
					cin >> cat;
					doc_estoque << "Categoria: ";
					doc_estoque << cat << endl;
				break;
				case 2:
					opt = 0;
				break;
				default:
					cout << "Opção inválida" << endl;
					opt = 1;
				break;
			}
		}
		cout << "Novo produto foi cadastrado" << endl << endl;
		doc_estoque.close();
	}
	else cout << "Não foi possível abrir a pasta" << endl;
	
	return;
}

void Interface(Socio * socio, vector <Produto*> produtos){
	cout << "1. Compra\n2. Recomendações\n3. Voltar ao menu principal" << endl;
	int opcoes;
	cin >> opcoes;
}

void Interface_funcionario(Funcionario * funcionario){
	
	int opc;
	cout << "1. Cadastrar produto" << endl;
	cin >> opc;
	
	switch (opc){
		case 1:
			Atualiza_estoque();
		break;
	}
}

void Login(){
	
	vector <Cliente*> clientes = LoadCliente();
	vector <Funcionario*> funcionarios = LoadFuncionario();
	vector <Socio*> socios = LoadSocio();
	vector <Produto*> produtos = LoadProduto();
	
	string temp;
	
	cout << "Digite o seu CPF:" << endl;
	cin >> temp;
	
	for(auto cliente:clientes){
		if(!cliente->get_cpf().compare("CPF:" + temp)){
			cout << "Cliente encontrado" << endl;
			Interface(cliente, produtos);
			return;
		}
	}
	for(auto funcionario:funcionarios){
		if(!funcionario->get_cpf().compare("CPF:" + temp)){
			cout << "Funcionário encontrado" << endl;
			Interface_funcionario(funcionario);
			return;
		}
	}
	for(auto socio:socios){
		if(!socio->get_cpf().compare("CPF:" + temp)){
			cout << "Sócio encontrado" << endl;
			Interface(socio, produtos);
			return;
		}
	}
	
	cout << "CPF não registrado" << endl;
	cout << "Retornando ao menu principal" << endl;
	return;
}

int main(int argc, char ** argv){
	
	int opc;

	cout << "Menu da Loja de Roupas Vic:" << endl;
	cout << "1. Cadastre-se" << endl;
	cout << "2. Entrar" << endl;
	cout << "3. Fechar programa" << endl;
	cin >> opc;
	
	if (opc == 1){
		if(!Cadastrar())
			main(argc, argv);
		cout << "Cadastro realizado com sucesso!" << endl;
		main(argc, argv);
	}
	if(opc == 2){
		Login();
		main(argc, argv);
	}
	if(opc == 3)	return 0;
	if(opc != 1 && opc != 2 && opc != 3){
		cout << "Opção inválida" << endl;
		cout << "Retornando ao menu principal" << endl;
		main(argc, argv);
	}

/*
	cout << "Qual é o nome do produto?" << endl;
	cin >> nome_produto;
	doc_estoque << "Produto: ";
	doc_estoque << nome_produto << endl;
	cout << "Quantas categorias ele tem?" << endl;
*/

/*
	cout << "Quantos produtos deseja adicionar?" << endl;
	cin >> quantidade;
	for(int cont = 0; cont < quantidade; cont++){
		cout << "Digite um novo produto:" << endl;
		cin >> nome;
		for(auto prod: estoque){
			if(prod->get_nome() == nome){
				prod->set_quantidade(prod->get_quantidade() + quantidade);
				produto_novo = false;
				cout << "O produto já foi cadastrado. Foi(ram) adicionad(os) mais" << quantidade << "unidades do mesmo";
				break;
			}
		}
		if (produto_novo == true){
			Produto * produto1 = new Produto();
			produto1->set_nome(nome);
			produto1->set_preco(preco);
*/


/*
			produto1->set_categoria(categoria);
*/
/*
			produto1->set_quantidade(quantidade);
		}
	}
*/
/*	fstream doc_cliente;
	string nome_cliente, cpf_cliente, email_cliente, telefone_cliente;
	int dia_nascimento_cliente, mes_nascimento_cliente, ano_nascimento_cliente;
	doc_cliente.open("doc/cliente.txt", ios::out | ios::in | ios::app);
	cout << "Qual é o seu nome? " << endl;
	cin >> nome_cliente;
	doc_cliente << "Nome: ";
	doc_cliente << nome_cliente << endl;
	cout << "Qual é o seu dia de nascimento? " << endl;
	cin >> dia_nascimento_cliente;
	doc_cliente << "Data de nascimento: ";
	doc_cliente << dia_nascimento_cliente << "/";
	cout << "Qual é o seu mês de nascimento? " << endl;
	cin >> mes_nascimento_cliente;
	doc_cliente << mes_nascimento_cliente << "/";
	cout << "Qual é o seu ano de nascimento? " << endl;
	cin >> ano_nascimento_cliente;
	doc_cliente << ano_nascimento_cliente << endl;
	cout << "Digite o seu CPF:" << endl;	
	cin >> cpf_cliente;
	doc_cliente << "CPF: ";
	doc_cliente << cpf_cliente << endl;
	cout << "Digite o seu email:" << endl;
	cin >> email_cliente;
	doc_cliente << "Email: ";
	doc_cliente << email_cliente << endl;	
	cout << "Digite o seu telefone:" << endl;
	cin >> telefone_cliente;
	doc_cliente << "Telefone: ";
	doc_cliente << telefone_cliente << endl << endl;
	doc_cliente.close();

	fstream doc_socio;
	string nome_socio, cpf_socio, email_socio, telefone_socio;
	int dia_nascimento_socio, mes_nascimento_socio, ano_nascimento_socio;
	doc_socio.open("doc/socio.txt", ios::out | ios::in | ios::app);
	cout << "Qual o seu nome?" << endl;
	cin >> nome_socio;
	doc_socio << "Nome: ";
	doc_socio << nome_socio << endl;
	cout << "Qual é o dia do seu nascimento?" << endl;
	cin >> dia_nascimento_socio;
	doc_socio << "Data de nascimento: ";
	doc_socio << dia_nascimento_socio << "/";
	cout << "Qual é o mês do seu nascimento?" << endl;
	cin >> mes_nascimento_socio;
	doc_socio << mes_nascimento_socio << "/";
	cout << "Qual é o ano do seu nascimento?" << endl;
	cin >> ano_nascimento_socio;
	doc_socio << ano_nascimento_socio << endl;
	cout << "Digite o seu CPF:" << endl;
	cin >> cpf_socio;
	doc_socio << "CPF: ";
	doc_socio << cpf_socio << endl;
	cout << "Digite o seu email:" << endl;
	cin >> email_socio;
	doc_socio << "Email: ";
	doc_socio << email_socio << endl;
	cout << "Digite o seu telefone:" << endl;
	cin >> telefone_socio;
	doc_socio << "Telefone: ";
	doc_socio << telefone_socio << endl << endl;
	doc_socio.close();


	fstream doc_funcionario;
	string nome_funcionario, cpf_funcionario, email_funcionario, funcao_funcionario, telefone_funcionario;
	float salario_funcionario;
	int dia_nascimento_funcionario, mes_nascimento_funcionario, ano_nascimento_funcionario, dia_contratacao_funcionario, mes_contratacao_funcionario, ano_contratacao_funcionario;
	doc_funcionario.open("doc/funcionario.txt", ios::out | ios::in | ios::app);
	cout << "Qual é o seu nome?" << endl;
	cin >> nome_funcionario;
	doc_funcionario << "Nome: ";
	doc_funcionario << nome_funcionario << endl;
	cout << "Qual é o dia do seu nascimento?" << endl;
	cin >> dia_nascimento_funcionario;
	doc_funcionario << "Data de nascimento: ";
	doc_funcionario << dia_nascimento_funcionario << "/";
	cout << "Qual é o mes do seu nascimento?" << endl;
	cin >> mes_nascimento_funcionario;
	doc_funcionario << mes_nascimento_funcionario << "/";
	cout << "Qual é o ano do seu nascimento?" << endl;
	cin >> ano_nascimento_funcionario;
	doc_funcionario << ano_nascimento_funcionario << endl;
	cout << "Digite o seu CPF:" << endl;
	cin >> cpf_funcionario;
	doc_funcionario << "CPF: ";
	doc_funcionario << cpf_funcionario << endl;
	cout << "Digite o seu email:" << endl;
	cin >> email_funcionario;
	doc_funcionario << "Email: ";
	doc_funcionario << email_funcionario << endl;
	cout << "Digite o seu telefone:" << endl;
	cin >> telefone_funcionario;
	doc_funcionario << "Telefone: ";
	doc_funcionario << telefone_funcionario << endl;
	cout << "Digite em (R$) qual é o seu salário, usando o ponto(.) para as casas decimais:" << endl;
	cin >> salario_funcionario;
	doc_funcionario << "Salário: " << fixed << setprecision(2);
	doc_funcionario << salario_funcionario << endl;
	cout << "Qual foi o dia em que foi contratado?" << endl;
	cin >> dia_contratacao_funcionario;
	doc_funcionario << "Data de contratação: ";
	doc_funcionario << dia_contratacao_funcionario << "/";
	cout << "Qual foi o mes em que foi contratado?" << endl;
	cin >> mes_contratacao_funcionario;
	doc_funcionario << mes_contratacao_funcionario << "/";
	cout << "Qual foi o ano em que foi contratado?" << endl;
	cin >> ano_contratacao_funcionario;
	doc_funcionario << ano_contratacao_funcionario << endl << endl;
	doc_funcionario.close();
*/
	

/*
	cout << "Em que categoria(s) ele se encontra?" << endl;
	cin >> nome_produto;
	doc_produto << "Categoria(s): ";
	doc_produto << nome_produto << endl;
	doc_produto.close();
*/	
/*
	Pessoa pessoa1;
	
	pessoa1.set_nome ("Gabi");
	pessoa1.set_dia_nascimento (10);
	pessoa1.set_mes_nascimento (06);
	pessoa1.set_ano_nascimento (1999);
	pessoa1.set_cpf (01425362545);
	pessoa1.set_email ("gabi@gmail.com");
	pessoa1.set_telefone (123456789);
	pessoa1.imprime_dados();
	cout << endl;


Funcionario funcionario1;
	
	funcionario1.set_nome ("Gabi");
	funcionario1.set_dia_nascimento (10);
	funcionario1.set_mes_nascimento (06);
	funcionario1.set_ano_nascimento (1999);
	funcionario1.set_cpf (01425362545);
	funcionario1.set_email ("gabi@gmail.com");
	funcionario1.set_telefone (123456789);
	funcionario1.set_salario (0.0);
	funcionario1.set_funcao ("Gerente");
	funcionario1.set_dia_da_contratacao (0);
	funcionario1.set_mes_da_contratacao (0);
	funcionario1.set_ano_da_contratacao (0);
	funcionario1.imprime_dados();
	cout << endl;
*/
	return 0;
}
