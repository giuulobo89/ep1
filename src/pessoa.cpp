#include "pessoa.hpp"
#include <iostream>

using namespace std;

Pessoa::Pessoa(){

    set_nome("");
    set_data_nascimento("00/00/0000");
    set_cpf("0");
    set_email("@email.com");
    set_telefone("00000000000");
    
}
Pessoa::~Pessoa(){
}

void Pessoa::set_nome(string nome){
    this->nome = nome;
}
string Pessoa::get_nome(){
    return nome;    
}
void Pessoa::set_data_nascimento(string data_nascimento){
    this->data_nascimento = data_nascimento;
}
string Pessoa::get_data_nascimento(){
    return data_nascimento;
}
void Pessoa::set_cpf(string cpf){
	this->cpf = cpf;
}
string Pessoa::get_cpf(){
	return cpf;
}
void Pessoa::set_email(string email){
    this->email = email;
}
string Pessoa::get_email(){
    return email;
}
void Pessoa::set_telefone(string telefone){
    //if(telefone % 100000000 || telefone > 99999999)
        //void Pessoa::set_telefone(int telefone);
    this->telefone = telefone;
}
string Pessoa::get_telefone(){
    return telefone;
}
void Pessoa::imprime_dados(){
    cout << nome << endl;
    cout << data_nascimento << endl;
    cout << cpf << endl;
    cout << email << endl;
    cout << telefone << endl << endl;
}
