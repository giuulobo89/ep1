#include "produto.hpp"
#include <iostream>

using namespace std;

Produto::Produto() {
    
    set_nome("");
    set_preco("0.0");
    set_quantidade("0");

}
Produto::~Produto() {
}

void Produto::set_nome(string nome){
    this->nome = nome;
}
string Produto::get_nome(){
    return nome;
}
void Produto::set_preco(string preco){
	this->preco = preco;
}
string Produto::get_preco(){
	return preco;
}
void Produto::set_quantidade(string quantidade){
	this->quantidade = quantidade;
}
string Produto::get_quantidade(){
	return quantidade;
}

void Produto::imprime_dados(){
	cout << nome << endl;
	cout << quantidade << endl;
	cout << preco << endl;
	for(auto categoria:categorias)
		cout << "Categoria: " << categoria << endl;
	cout << endl << endl;
}

void Produto::inserir_categoria(string categoria){
	categorias.push_back(categoria);
}
