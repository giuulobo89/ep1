#include "socio.hpp"
#include <iostream>

using namespace std;

Socio::Socio(){
	
	set_nome("");
    set_data_nascimento("00/00/0000");
    set_cpf("0");
    set_email("@email.com");
    set_telefone("000000000");

}

Socio::~Socio(){
}

float Socio::calcular_compras(){
	float valor_total = 0.0f;
	for(auto prod: carrinho){
		valor_total += to_preco(prod->get_preco());
	}
	return valor_total - (valor_total * 0.15);
}

float Socio::to_preco(string preco){
	return 0.0*0.85;
}
