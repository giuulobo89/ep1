#include <iostream>
#include "funcionario.hpp"

using namespace std;

Funcionario::Funcionario(){
    
    set_nome("");
    set_data_nascimento("00/00/0000");
    set_cpf("0");
    set_email("@email.com");
    set_telefone("000000000");
	set_salario("");
    set_funcao("");
    set_data_da_contratacao("00/00/0000");
}
Funcionario::~Funcionario(){
    
}
string Funcionario::get_salario(){
    return salario;
}
void Funcionario::set_salario(string salario){
    this->salario = salario;
}
string Funcionario::get_funcao(){
    return funcao;
}
void Funcionario::set_funcao(string funcao){
    this->funcao = funcao;
}
string Funcionario::get_data_da_contratacao(){
    return data_da_contratacao;
}
void Funcionario::set_data_da_contratacao(string data_da_contratacao){
    this->data_da_contratacao = data_da_contratacao;
}
void Funcionario::imprime_dados(){
    cout << get_nome() << endl;
    cout << get_data_nascimento() << endl;
    cout << get_cpf() << endl;
    cout << get_email() << endl;
    cout << get_telefone() << endl;
    cout << salario << endl;
    cout << data_da_contratacao << endl << endl;
}
