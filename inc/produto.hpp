#ifndef PRODUTO_HPP
#define PRODUTO_HPP
#include <string>
#include <set>
#include <fstream>
#include <string>
#include <iomanip>
#include <vector>

using namespace std;

class Produto {
    private:
    string nome;
    string preco;
    string quantidade;
	vector <string> categorias;
    
    public:
    Produto();
    ~Produto();
    
    void set_nome(string nome);
	string get_nome();
	void set_preco(string preco);
	string get_preco();
	void set_quantidade(string quantidade);
	string get_quantidade();
	void imprime_dados();
	void inserir_categoria(string categoria);
};

#endif
