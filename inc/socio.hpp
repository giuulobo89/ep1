#ifndef SOCIO_HPP
#define SOCIO_HPP
#include <string>
#include "cliente.hpp"

class Socio: public Cliente{
    private:
    public:
	Socio();
	~Socio();
    float calcular_compras();
	float to_preco(string preco);
};

#endif
