#include <iostream>
#include "cliente.hpp"
#include "funcionario.hpp"
#include "socio.hpp"
#include "produto.hpp"
#include <fstream>
#include <string>
#include <iomanip>

bool Cadastrar(){
	int opcao;
	cout << "1. Cliente" << endl;
	cout << "2. Funcionário" << endl;
	cin >> opcao;
	
	if(opcao == 1){
		cout << "1. Cliente Sócio" << endl;
		cout << "2. Cliente Comum" << endl;
		cin >> opcao;
		if(opcao == 1)
			opcao = 3;
		else
			opcao = 1;
	}
	if(opcao != 2 && opcao != 1){
		cout << "Opção inválida" << endl;
		cout << "Voltando ao menu principal..." << endl;
		return false;
	}
	
	
	fstream doc_cadastrar;
	
	switch(opcao){
		case 1:	
			doc_cadastrar.open("doc/cliente.txt", ios::out | ios::in | ios::app);
		break;
		case 2:	
			doc_cadastrar.open("doc/funcionario.txt", ios::out | ios::in | ios::app);
		break;
		case 3:	
			doc_cadastrar.open("doc/socio.txt", ios::out | ios::in | ios::app);
		break;
		default:
			cout << "Opção inválida" << endl;
			cout << "Voltando ao menu principal..." << endl;
			return false;
		break;
	}
	string temp;
	cout << "Primeiro nome: " << endl;
	cin >> temp;
	doc_cadastrar << "Nome: " << temp << endl;
	
	cout << "Data de nascimento: XX/XX/XXXX " << endl;
	cin >> temp;
	doc_cadastrar << "Data de nascimento: " << temp << endl;
	
	cout << "Digite o seu CPF: 11 dígitos(somente números)" << endl;
	cin >> temp;
	doc_cadastrar << "CPF:" << temp << endl;
	
	cout << "Digite o seu email:" << endl;
	cin >> temp;
	doc_cadastrar << "Email: " << temp << endl;
	
	cout << "Digite o seu telefone: 11 Dígitos" << endl;
	cin >> temp;
	doc_cadastrar << "Telefone: " << temp << endl;
	
	if(opcao == 2){
		cout << "Digite o seu salário: R$XX.XX" << endl;
		cin >> temp;
		doc_cadastrar << "Salário: " << temp << endl;
		
		cout << "Data de contratação: XX/XX/XXXX" << endl;
		cin >> temp;
		doc_cadastrar << "Salário: " << temp << endl << endl;
	}
	else
		doc_cadastrar << endl;
	
	doc_cadastrar.close();
	return true;

}

vector <Cliente*> LoadCliente(){
	
	fstream doc_cliente;
	doc_cliente.open("doc/cliente.txt", ios::out | ios::in | ios::app);
	
	vector <Cliente*> clientes;
	string linhas;
	int contador = 0;
	
	while(getline(doc_cliente,linhas)){
		clientes.push_back(new Cliente());
		clientes[contador]->set_nome(linhas);	
		
		getline(doc_cliente,linhas);
		clientes[contador]->set_data_nascimento(linhas);
		
		getline(doc_cliente,linhas);
		clientes[contador]->set_cpf(linhas);
		
		getline(doc_cliente,linhas);
		clientes[contador]->set_email(linhas);
		
		getline(doc_cliente,linhas);
		clientes[contador]->set_telefone(linhas);
		
		getline(doc_cliente,linhas);
		contador++;
	}
	
	doc_cliente.close();
	
	return clientes;
}

vector <Funcionario*> LoadFuncionario(){
	
	fstream doc_funcionario;
	doc_funcionario.open("doc/funcionario.txt", ios::out | ios::in | ios::app);
	
	vector <Funcionario*> funcionarios;
	string linhas;
	int contador = 0;
	
	while(getline(doc_funcionario,linhas)){
		funcionarios.push_back(new Funcionario());
		funcionarios[contador]->set_nome(linhas);	

		getline(doc_funcionario,linhas);
		funcionarios[contador]->set_data_nascimento(linhas);

		getline(doc_funcionario,linhas);
		funcionarios[contador]->set_cpf(linhas);

		getline(doc_funcionario,linhas);
		funcionarios[contador]->set_email(linhas);

		getline(doc_funcionario,linhas);
		funcionarios[contador]->set_telefone(linhas);

		getline(doc_funcionario,linhas);
		funcionarios[contador]->set_salario(linhas);

		getline(doc_funcionario,linhas);
		funcionarios[contador]->set_data_da_contratacao(linhas);
		
		getline(doc_funcionario,linhas);
		contador++;
	}
	
	doc_funcionario.close();
	
	return funcionarios;
}

vector <Socio*> LoadSocio(){
	
	fstream doc_socio;
	doc_socio.open("doc/socio.txt", ios::out | ios::in | ios::app);
	
	vector <Socio*> socios;
	string linhas;
	int contador = 0;
	
	while(getline(doc_socio,linhas)){
		socios.push_back(new Socio());
		socios[contador]->set_nome(linhas);	

		getline(doc_socio,linhas);
		socios[contador]->set_data_nascimento(linhas);

		getline(doc_socio,linhas);
		socios[contador]->set_cpf(linhas);

		getline(doc_socio,linhas);
		socios[contador]->set_email(linhas);

		getline(doc_socio,linhas);
		socios[contador]->set_telefone(linhas);

		getline(doc_socio,linhas);
		contador++;
	}
	
	doc_socio.close();
	
	return socios;
}

vector <Produto*> LoadProduto(){
	
	fstream doc_produto;
	doc_produto.open("doc/estoque.txt", ios::out | ios::in | ios::app);
	
	vector <Produto*> produtos;
	string linhas;
	int contador = 0;
	
	while(getline(doc_produto,linhas)){
		produtos.push_back(new Produto());
		produtos[contador]->set_nome(linhas);

		getline(doc_produto,linhas);
		produtos[contador]->set_quantidade(linhas);

		getline(doc_produto,linhas);
		produtos[contador]->set_preco(linhas);
		
		while(true){
			getline(doc_produto, linhas);
			if(!linhas.compare("EOP")){
				break;
			}
			produtos[contador]->inserir_categoria(linhas);
		}
		getline(doc_produto,linhas);
		contador++;
	}
	
	doc_produto.close();
	
	return produtos;
}
