#ifndef CLIENTE_HPP
#define CLIENTE_HPP
#include "pessoa.hpp"
#include <string>
#include <vector>
#include "produto.hpp"

using namespace std;

class Cliente:public Pessoa {
    
	public:
	Cliente();
	~Cliente();
	vector <Produto*> carrinho;

	void inserir_produtos(Produto * produto);
	void inserir_produtos();
	float to_preco(string preco);
	virtual float calcular_compras();
	void imprime_dados();
};

#endif
