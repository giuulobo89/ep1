#ifndef FUNCIONARIO_HPP
#define FUNCIONARIO_HPP
#include <string>
#include "pessoa.hpp"

class Funcionario: public Pessoa{
    private:
        string salario;
        string funcao;
        string data_da_contratacao;

    public:
        Funcionario();
        ~Funcionario();
        string get_salario();
        void set_salario(string salario);
        string get_funcao();
        void set_funcao(string funcao);
        string get_data_da_contratacao();
        void set_data_da_contratacao(string data_da_contratacao);
		void imprime_dados();
        
};

#endif
